
Assembling
```
llvm-mc blink.a -o blink_llvm.o --arch=riscv32 -filetype=obj -g

```
Linking

```
ld.lld -T gd32vf103cb.ld -o blink_llvm.elf blink_llvm.o -m elf32lriscv
```
Launching openocd
```
riscv64-linux-gnu-openocd -f sipeed-rv-debugger.cfg
```
Launching gdb
```
riscv64-elf-gdb
```
Updating
```
(gdb) target extended-remote :3333
(gdb) monitor flash erase_sector 0 0 127
(gdb) monitor flash write_image blink_llvm.elf
```
Debugging
```
(gdb) file blink_llvm.elf
(gdb) target extended-remote :3333
(gdb) load
...
```
