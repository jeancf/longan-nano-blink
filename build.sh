#!/bin/sh

echo "Compiling..."
llvm-mc blink.a -o blink_llvm.o --arch=riscv32 -filetype=obj -g
echo "Linking..."
ld.lld -T gd32vf103cb.ld -o blink.elf blink_llvm.o -m elf32lriscv
echo "blink.elf ready."
