# Place this file in your home directory
# to be sourced by gdb at startup

# command "upload_blink"
define upload_blink
    echo Connecting to target...\n
    target extended-remote :3333
    echo Erasing flash...\n
    monitor flash erase_sector 0 0 127
#    echo Verifying flash is erased...\n
#    monitor flash erase_check 0
    echo Writing image...\n
    monitor flash write_image blink.elf
    echo Verifying written image...\n
    monitor flash verify_image blink.elf
    echo Done. Press the reset button to run.\n
end

define debug_blink
    file blink.elf
    target extended-remote :3333
    load
end
